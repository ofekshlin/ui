const fs = require('fs')

async function newPool(){
    var users = [];
    await fs.readFile('./names.txt', 'utf8' , (err, data) => {
      if (err) {
        console.error(err)
        return
      }
      console.log(data.split('\n'));
      data.split('\n').forEach((element) => {
          users.push(element);
        });
        const finishedData = JSON.stringify({ "users" : users });
        
        fs.writeFile('userspool.json', finishedData, 'utf8', (err) => {
            console.log(err);
        });
    });
}

function readPool(){
    return JSON.parse(fs.readFileSync('./userspool.json', 'utf8'))["users"];
}

async function createNewToranimList(toranim, name, pool, number ){   
    while(toranim[name].length < number){
        const rnd = Math.floor(Math.random() * pool.length);
        const newName = pool[rnd];
        pool.splice(rnd, 1);
        if(newName in toranim[name]){
            pool.push(newName);
            continue
        }
        toranim[name].push(newName);
        if(pool.lengh == 0){
            await newPool();
        }
    }
    return toranim, pool
}



async function main(){
    let pool = readPool();
    let toranim = {"kk": [], "reserve": []};
    toranim, pool = await createNewToranimList(toranim, "kk", pool, 4);
    toranim, pool = await createNewToranimList(toranim, "reserve", pool, 2);
    fs.writeFile('userspool.json', JSON.stringify({"users" : pool}), 'utf8', (err) => {
        console.log(err);
    });
    fs.writeFile('toranim.json', JSON.stringify(toranim), 'utf8', (err) => {
        console.log(err);
    });
}