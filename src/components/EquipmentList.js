import * as React from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import equipment from '../jsons/equipment.json';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
    textAlign: "center"
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
    direction: "rtl",
    textAlign: "center"
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

export default function EquipmentList() {
    return (
    <TableContainer component={Paper}>
      <Table sx={{ width: "100vw" }} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>רשימת ציוד</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {equipment["equip"].map((row) => (
            <StyledTableRow key={row}>
              <StyledTableCell component="th" scope="row">
                {row[0]}
              </StyledTableCell>
              <StyledTableCell component="th" scope="row">
                {row[1]}
              </StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}