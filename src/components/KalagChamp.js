import * as React from 'react';
import Chart from "react-google-charts";
import users from '../jsons/users.json'; 
import {useEffect, useState } from 'react';

export default function KalagChamp() {
  const [data, setData] = useState([]);

  useEffect(() => {
    const newData = [["players", "score"]]; 
    users["users"].forEach((elm) => newData.push([elm["name"], elm["score"]]));
    setData(newData);
  }, []);

  return (
      <Chart
          width={'inherit'}
          style={{overflow: "scroll"}}
          height={"85vh"}
          chartType="BarChart"
          loader={<div>Loading Chart</div>}
          data={data}
          // For tests
          // rootProps={{ 'data-testid': '2' }}
          chartPackages={['corechart', 'controls']}
          legendToggle
      />
  );
}


/* [
              {
                controlType: 'NumberRangeFilter',
                options: {
                  filterColumnIndex: 1,
                  minValue: 0,
                  maxValue: 60,
                },
              },
              {
                controlType: 'StringFilter',
                options: {
                  filterColumnIndex: 0,
                  matchType: 'any', // 'prefix' | 'exact',
                  ui: {
                    label: 'Search by name',
                  },
                },
              }
            ])*/