import * as React from 'react';
import Box from '@mui/material/Box';
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import LocalPostOfficeIcon from '@mui/icons-material/LocalPostOffice';
import AttachFileIcon from '@mui/icons-material/AttachFile';
import BarChartIcon from '@mui/icons-material/BarChart';
import NoteAddIcon from '@mui/icons-material/NoteAdd';

export default function Menubar({changeField}) {
  const [value, setValue] = React.useState(0);

  return (
    <Box>
      <BottomNavigation
        showLabels
        value={value}
        onChange={(event, newValue) => {
          setValue(newValue);
          changeField(newValue);
        }}
      >
        <BottomNavigationAction label="Messages" icon={<LocalPostOfficeIcon />} />
        <BottomNavigationAction label="Files" icon={<AttachFileIcon />} />
        <BottomNavigationAction label="Champ" icon={<BarChartIcon />} />
        <BottomNavigationAction label="Forms" icon={<NoteAddIcon />} />
        
      </BottomNavigation>
    </Box>
  );
}
