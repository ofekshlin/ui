import * as React from 'react';
import SimpleMessage from './SimpleMessage';
import { List } from '@mui/material';
import messages from '../jsons/messages.json';

export default function MessageBoard() {
  return (
    <div>
        <List spacing={2} style={{ height: "inherit"}}>
            {messages["messages"].map((message) => {
              return (
                <SimpleMessage title={message["title"]} body={message["body"]} />
              )
            })}
        </List>
    </div>
  );
}