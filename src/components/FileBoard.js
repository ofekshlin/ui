import * as React from 'react';
import { List } from '@mui/material';
import File from './File'
import files from '../jsons/files.json';

export default function FileBoard() {
  return (
    <div>
        <List spacing={2} style={{ height: "85vh", overflow: "scroll"}}>
            {files["files"].map(
              (file) => {
                return(
                  <File title={file.title} filePath={file.url} />
                )
              }
            )}
        </List>
    </div>
  );
}