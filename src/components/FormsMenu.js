import * as React from 'react';
import Paper from '@mui/material/Paper';
import MenuList from '@mui/material/MenuList';
import MenuItem from '@mui/material/MenuItem';
import ListItemText from '@mui/material/ListItemText';
import Typography from '@mui/material/Typography';
import forms from '../jsons/forms.json';

export default function FormsMenu() {

  return (
    <Paper sx={{ maxWidth: '100%' }}>
      <MenuList>
          {
              forms["links"].map((link) => {
                return (
                <a href={link["link"]} style={{textDecoration:"none", color: "black"}}>
                <MenuItem>
                    <ListItemText>
                      <Typography>{link["name"]}</Typography>
                    </ListItemText>
                </MenuItem>
                </a>)
              })
          }
      </MenuList>
    </Paper>
  );
}