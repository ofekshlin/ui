import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Tooltip from '@mui/material/Tooltip';
import ContactPageIcon from '@mui/icons-material/ContactPage';
import '../App.css'
import { IconButton } from '@mui/material';
import PropTypes from 'prop-types';
import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import CloseIcon from '@mui/icons-material/Close';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
    },
    }));

const BootstrapDialogTitle = (props) => {
    const { children, onClose, ...other } = props;
    return (
        <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
        {children}
        {onClose ? (
            <IconButton
            aria-label="close"
            onClick={onClose}
            sx={{
                position: 'relative',
                color: (theme) => theme.palette.grey[500],
            }}
            >
            <CloseIcon />
            </IconButton>
        ) : null}
        </DialogTitle>
    );
};

BootstrapDialogTitle.propTypes = {
children: PropTypes.node,
onClose: PropTypes.func.isRequired,
};

export default function Mainbar({title}) {
    const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Box>
      <AppBar position="static">
        <Toolbar className="MainToolBar">
            <Typography
                variant="h6"
                noWrap
                component="div"
            >
                {title}
            </Typography>

            <Box >
                <Tooltip title="Contact Us">
                    <IconButton onClick={handleClickOpen} sx={{ p: 0, color: (theme) => theme.palette.common.white}} >
                        <ContactPageIcon />
                    </IconButton>
                </Tooltip>
            </Box>
        </Toolbar>
      </AppBar>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
          צור קשר
        </BootstrapDialogTitle>
        <DialogContent dividers>
          הקלג: אור שחר<br />
          מספר טלפון: 0525777090<br />
          <br />
          מפתח המערכת: אופק שלינגר<br />
          מספר טלפון: 0558828130<br />
        </DialogContent>
      </BootstrapDialog>
    </Box>
    
  );
}