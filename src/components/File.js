import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Button, CardActions } from '@mui/material';
import { saveAs } from 'file-saver';

export default function File({title, filePath}) {
  const downloadImage = () => {
    saveAs(filePath, title + '.jpg');
  }

    return (
    <Card >
        <CardMedia
        component="img"
        height="auto"
        src={filePath}
      />
        <CardContent>
          <Typography gutterBottom variant="h6" component="div">
            {title}
          </Typography>
        </CardContent>
      <CardActions>
        <Button size="small" color="primary" onClick={downloadImage}>
          Download
        </Button>
      </CardActions>
    </Card>
  );
}