import * as React from 'react';
import './App.css';
import FileBoard from './components/FileBoard';
import Mainbar from'./components/Mainbar'
import Menubar from './components/Menubar';
import Strings from './Strings.json';
import KalagChamp from './components/KalagChamp';
import FormsMenu from './components/FormsMenu';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import InfoPage from './components/InfoPage';


const theme = createTheme({
  palette: {
    primary: {
      main: '#DC143C'
    }
  }
});

function App() {
  const [index, setIndex] = React.useState(0);
  
  return (
    <ThemeProvider theme={theme}>
    <div className="App">
        <Mainbar className="Mainbar" title={Strings.title}></Mainbar>
        <div  hidden={index !== 0}><InfoPage /></div>
        <div  hidden={index !== 1}><FileBoard/></div>
        <div  hidden={index !== 2}><KalagChamp></KalagChamp></div>
        <div  hidden={index !== 3}><FormsMenu></FormsMenu></div>
        <div className="Menubar"><Menubar changeField={setIndex}></Menubar></div>
    </div>
    </ThemeProvider>
  );
}

export default App;
