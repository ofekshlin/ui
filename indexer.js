const firebaseConfig = {
  apiKey: "AIzaSyAw5w8DoKRtGrmfm3TWd08aFpFVT3y8SSA",
  authDomain: "brosh75-bcdac.firebaseapp.com",
  databaseURL: "https://brosh75-bcdac-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "brosh75-bcdac",
  storageBucket: "brosh75-bcdac.appspot.com",
  messagingSenderId: "568845041639",
  appId: "1:568845041639:web:e3defcd3c18b988d5e8742",
  measurementId: "G-SZ17H65Q5E"
};

// Import Admin SDK
const { getDatabase } = require('firebase-admin/database');

var admin = require("firebase-admin");

// Initialize the app with a service account, granting admin privileges
admin.initializeApp(firebaseConfig);


// Get a database reference to our blog
const db = getDatabase();
var ref = db.ref("/brosh75-bcdac-default-rtdb");
const usersRef = ref.child('users');
usersRef.set({
  alanisawesome: {
    date_of_birth: 'June 23, 1912',
    full_name: 'Alan Turing'
  },
  gracehop: {
    date_of_birth: 'December 9, 1906',
    full_name: 'Grace Hopper'
  }
});